
# Use the official Node runtime as a parent image
FROM node:slim
# Set the working directory to /gpsdevnode
WORKDIR /gpsdevnode
# Copy the current directory contents into the container at /gpsdevnode
ADD . /gpsdevnode
# Install any needed packages
# RUN npm install
# Run app.js when the container launches
CMD ["node", "serverMQ.js"]
# docker run -d --name device3 --network="host"  virtualdevices node app.js --id 0358899050246303 --file List3.txt
