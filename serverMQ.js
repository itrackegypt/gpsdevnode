//Load the TCP Library
var net = require('net');
//Load the GeoJSON Utilities for JavaScript
var gju = require('geojson-utils');
var inside = require('point-in-geopolygon');
//Load library that resolves location into address
var geocoder = require('geocoder');

var Utilities = require('./utilities');
var miscUtils = new Utilities();
// Load mongodb library
var MongoClient = require('mongodb').MongoClient;

// mongod/mongos server location & port
var mongodServer = 'mongodb://mongodb-traccar:27017/iTrackEgypt';
//var mongodServer = 'mongodb://146.148.69.177:27017/iTrackEgypt';

// server listening port
var portNum = 3000;

// creating an object to hold connection information for a connected device
function connection(socket, name, remoteAddress, remotePort, deviceIMEI) {
    this.socket = socket;
    this.name = name;
    this.remoteAddress = remoteAddress;
    this.remotePort = remotePort;
    this.deviceIMEI = deviceIMEI;
}

// Keep track of the connection information of each connected device 
var devicesConnections = [];// an array of 'connection' data type

/**
 * Takes in the Heartbeat message and returns a document with most of the keys of the 'updates' collection & their
 * corresponding values
 * @param {String} inMessage : the message sent by the device in Hexadecimal encoding
 * @returns: {Object} newUpdate : a key value pair containing the following keys of the 'updates' collection & their
 * corresponding values:
	 * <br>deviceIMEI: String
	 * <br>updateType: String = heartbeat
	 * <br>serverTime: Date
	 * <br>voltageLevel: 32-bit int
	 * <br>gsmSignal: 32-bit int
	 * <br>packetNumber: 32-bit int
	 * <br>positioningState: 32-bit int
	 * <br>satellitesNumber: 32-bit int
	 * <br>satellitesSNR: Array of 32-bit int
 * @note: the remaining key that needs to be set is: stopFlag
 */
function parseHeartbeatMessage(inMessage, socket){
	var newUpdate = new Object();
	
	newUpdate.socketRemoteAddress = socket.remoteAddress;
	newUpdate.socketRemotePort = socket.remotePort;
	
	newUpdate.updateType = "heartbeat";
	newUpdate.serverTime = new Date();
	
	var packetLength = parseInt(inMessage.slice(4,6),16);
	newUpdate.voltageLevel = parseInt(inMessage.slice(6,8),16);
	newUpdate.gsmSignal = parseInt(inMessage.slice(8,10),16);
	newUpdate.deviceIMEI = inMessage.slice(10,26);
	newUpdate.packetNumber = parseInt(inMessage.slice(26,30),16);
	newUpdate.positioningState = parseInt(inMessage.slice(32,34),16);
	newUpdate.satellitesNumber = parseInt(inMessage.slice(34,36),16);
	newUpdate.satellitesSNR = [];
	var start = 36
	for (var i = 0; i < newUpdate.satellitesNumber; i++) {
		var satelliteSNR = parseInt(inMessage.slice(start,start+2),16);
		start = start + 2;
		newUpdate.satellitesSNR.push(satelliteSNR);
	}
	
	return newUpdate;
}

/**
 * Update the document of the specified device in the 'devices' collection, add / update a document in the
 * 'events' collection in case of a stop event & add a new Heartbeat update to the 'updates' collection
 * @param {Object} newUpdate : a key value pair containing the following keys of the 'updates' collection & their
 * corresponding values:
	 * <br>deviceIMEI: String
	 * <br>updateType: String = heartbeat
	 * <br>serverTime: Date
	 * <br>voltageLevel: 32-bit int
	 * <br>gsmSignal: 32-bit int
	 * <br>packetNumber: 32-bit int
	 * <br>positioningState: 32-bit int
	 * <br>satellitesNumber: 32-bit int
	 * <br>satellitesSNR: Array of 32-bit int
 * @param {Number} stopDetailThreshold : the minimum time in milliseconds to trigger a stop event (time between
 * this heartbeat and the last update)
 * @param {Db} db : mongodb database handle
 */
function handleHearbeatUpdate(newUpdate, stopDetailThreshold, db){
	
	var projection = {
			'_id' : 0,
			'lastUpdateTime' : 1,
			'stopDetailTimerFlag' : 1,
			'lastUpdateLocation' : 1
			};
	
	db.collection("devices").findOne({ "_id" : newUpdate.deviceIMEI }, function(err, doc) {
		if(err) {
			throw err;
		}
		
		if(!doc) { // document doesn't exist 
			console.log('Heartbeat handle: No documents for device IMEI ' + newUpdate.deviceIMEI + ' was found in the database!');
		}else{ // document exists
			var query = { "_id" : newUpdate.deviceIMEI };
			
			// Add new document or update existing one in 'events' collection in case of a stop event
			if (!doc.offlineFlag && newUpdate.serverTime.getTime() - doc.lastUpdateTime.getTime() > stopDetailThreshold ){// Check for a 'stop' event
				newUpdate.stopFlag = true;
				if (!doc.stopDetailTimerFlag){// stop event flag wasn't activated before this Heartbeat
					var updateOperators = {'$set' : { "stopDetailTimerFlag" : true}};
					db.collection('devices').update(query, updateOperators, function(err, updated) {
						if(err) {throw err;}
						console.dir("Successfully updated the document of device IMEI " + newUpdate.deviceIMEI);
					});
					
					var newStopEventDoc = {
							"deviceIMEI": newUpdate.deviceIMEI,
							"eventType": "stop",
							"eventTime": newUpdate.serverTime,//doc.lastUpdateTime,
							"timestamps": [doc.lastUpdateTime],
							"location": doc.lastUpdateLocation
							};
					db.collection('events').insert(newStopEventDoc, function(err, inserted) {
				        if(err) throw err;
				        console.dir("Successfully inserted new stop event: " + JSON.stringify(inserted));
				    });
				}else{// stop event flag was activated in a previous heartbeat
					var eventsQuery = { "deviceIMEI": newUpdate.deviceIMEI, "eventType": "stop"};
				    var sort = [["eventTime",-1]];
				    var operator = { "$push" : { "timestamps" : newUpdate.serverTime } };
				    var options = { 'new' : false };

				    db.collection('events').findAndModify(eventsQuery, sort, operator, options, function(err, doc) {
				        if(err) throw err;
				    });
				}
			}
			
			// Add a new document containing the received information to the updates collection
			db.collection("updates").insert(newUpdate, function(err, inserted) {
				if(err) {throw err;}
				console.dir("Successfully inserted a Heartbeat update to the 'updates' collection for device " + newUpdate.deviceIMEI);
			});
		}
	});
}

/**
 * Takes in a data message and returns a document with most of the keys of the 'updates' collection & their
 * corresponding values
 * @param {String} str : the update type = 'gps' or 'address'
 * @param {String} inMessage : the message sent by the device in Hexadecimal encoding
 * @returns: {Object} newUpdate : a key value pair containing the following keys of the 'updates' collection & their
 * corresponding values:
	 * <br>deviceIMEI: String
	 * <br>updateType: String = gps
	 * <br>deviceTime: Date
	 * <br>serverTime: Date
	 * <br>location: GeoJSON object
	 * <br>speed: 32-bit int
	 * <br>direction: 32-bit int
	 * <br>packetNumber: 32-bit int
	 * <br>locationAreaCode: 32-bit int
	 * <br>mobileNetworkCode:32-bit int
	 * <br>cellTowerID: 32-bit int
	 * <br>eastLongitude: Boolean
	 * <br>northLatitude: Boolean
	 * <br>gpsLocated: Boolean
 * @note: the remaining keys that need to be set are: overspeed, totalMileage and insideGeofence.
 */
function parseGpsOrAddressMessage(str,inMessage,socket){
	var newUpdate = new Object();
	
	newUpdate.socketRemoteAddress = socket.remoteAddress;
	newUpdate.socketRemotePort = socket.remotePort;
	
	newUpdate.updateType = str;
	newUpdate.serverTime = new Date();
	
	newUpdate.locationAreaCode = parseInt(inMessage.slice(6,10),16);
	newUpdate.deviceIMEI = inMessage.slice(10,26);
	newUpdate.packetNumber = parseInt(inMessage.slice(26,30),16);
	//newUpdate.protocolNum = inMessage.slice(30,32);
	var dateTimeHex = inMessage.slice(32,44);
	var yearDec = parseInt(dateTimeHex.slice(0,2),16);
	var yearString = yearDec<10? '0'+yearDec : yearDec;
	var monthDec = parseInt(dateTimeHex.slice(2,4),16);
	var monthString = monthDec<10? '0'+monthDec : monthDec;
	var dayDec = parseInt(dateTimeHex.slice(4,6),16);
	var dayString = dayDec<10? '0'+dayDec : dayDec;
	var hourDec = parseInt(dateTimeHex.slice(6,8),16);
	var hourString = hourDec<10? '0'+hourDec : hourDec;
	var minutesDec = parseInt(dateTimeHex.slice(8,10),16);
	var minutesString = minutesDec<10? '0'+minutesDec : minutesDec;
	var secondsDec = parseInt(dateTimeHex.slice(-2),16);
	var secondsString = secondsDec<10? '0'+secondsDec : secondsDec;
	var dateTimeString = '20'+yearString+'-'+monthString+'-'+dayString+'T'+hourString+':'+minutesString+':'+secondsString+'.000Z';
	newUpdate.deviceTime = new Date(dateTimeString.toString());
	var latitudeHex = inMessage.slice(44,52);
	var latitudeDec = parseInt(latitudeHex,16)/1800000;
	var longitudeHex = inMessage.slice(52,60);
	var longitudeDec = parseInt(longitudeHex,16)/1800000;
	newUpdate.location = { type: "Point", coordinates: [ longitudeDec, latitudeDec] };
	var speedHex = inMessage.slice(60,62);
	newUpdate.speed = parseInt(speedHex,16);
	var directionHex = inMessage.slice(62,66);
	newUpdate.direction = parseInt(directionHex,16) & 1023;
	newUpdate.mobileNetworkCode = parseInt(inMessage.slice(66,68),16);
	newUpdate.cellTowerID = parseInt(inMessage.slice(68,72),16);
	var statusInfo = inMessage.slice(78,80);
	newUpdate.eastLongitude = (parseInt(statusInfo,16) & 4)? true:false;
	newUpdate.northLatitude = (parseInt(statusInfo,16) & 2)? true:false;
	newUpdate.gpsLocated = (parseInt(statusInfo,16) & 1)? true:false;
	
	return newUpdate;
}

/**
 * Inserts a new document representing a new device in the 'devices' collection while adding a new update to the
 * 'updates' collection
 * @param newUpdate containing most of the keys of the 'updates' collection & their values except the
 * following keys that need to be set:
	 * <br>overspeed
	 * <br>totalMileage
 * @param {Db} db : mongodb database handle
 */
function createNewDevice(newUpdate, db){
	var device = {_id:newUpdate.deviceIMEI};
	device.type = 'TR02';
	device.fuelConsumption100KM = 20;
	device.overspeed = 80;
	device.geofences = [];
	device.totalMileage = 0.0;
	newUpdate.totalMileage=device.totalMileage;
	device.offlineFlag = false;
	device.stopDetailTimerFlag = false;
	device.oilMileageThreshold = 5000;
	device.tyreMileageThreshold = 6000;
	device.brakeMileageThreshold = 7000;
	device.lastUpdateTime = newUpdate.serverTime;
	device.lastUpdateLocation = newUpdate.location;
	device.lastUpdateSpeed = newUpdate.speed;
	device.lastUpdateDirection = newUpdate.direction;
	device.users = [];
	
	// add new document in the 'events' collection in case of an overspeed event
	if(newUpdate.speed>device.overspeed){
		newUpdate.overspeed = true;
		device.lastUpdateOverspeedState = true ;
		var newOverspeedEventDoc = {
				"deviceIMEI": newUpdate.deviceIMEI,
				"eventType": "overspeed",
				"eventTime": newUpdate.serverTime,
				"timestamps": [newUpdate.serverTime],
				"locations": [newUpdate.location],
				"speedValues": [newUpdate.speed]
		};
		db.collection('events').insert(newOverspeedEventDoc, function(err, inserted) {
	        if(err) throw err;
	        console.dir("Successfully inserted a new overpeed event: " + JSON.stringify(inserted));
	    });
	}else{
		newUpdate.overspeed = false;
	}
	
	
	
	// Insert new device to the 'devices' collection
	db.collection('devices').insert(device, function(err, inserted) {
        if(err) throw err;
        console.dir("Successfully inserted new device: " + JSON.stringify(inserted));
    });
	
	// Add a new document containing the received information to the updates collection
	db.collection("updates").insert(newUpdate, function(err, inserted) {
		if(err) {throw err;}
		console.dir("Successfully inserted a GPS update to the 'updates' collection for device " + newUpdate.deviceIMEI);
	});
}

/**
 * Uses the GPS update to check for an overspeed event, an offline event, a stop event or a geofence event then adds new
 * documents representing those events in the 'events' collection. The funtion also updates the device's document in the
 * 'devices' collection & adds a new update to the 'updates' collection 
 * @param {Object} newUpdate containing most of the keys of the 'updates' collection & their values except the
 * following keys that need to be set:
	 * <br>overspeed
	 * <br>totalMileage
	 * <br>insideGeofence.
 * @param {Object} doc : The document of the specified device in the 'devices' collection
 * @param {Db} db : mongodb database handle
 */
function checkEventsAndUpdate(newUpdate, doc, db){
	var query = { "_id" : newUpdate.deviceIMEI };
	var updateOperators = {
			"$set" : {
				"lastUpdateTime" : newUpdate.serverTime ,
				"lastUpdateLocation" : newUpdate.location,
				"lastUpdateSpeed" : newUpdate.speed,
				},
			"$push" : {},
			"$inc" : {},
			"$pull" : {}
	};
	
	// add new document or update existing document in the 'events' collection in case of an overspeed event
	if(newUpdate.speed>doc.overspeed){
		newUpdate.overspeed = true;
		if (!doc.lastUpdateOverspeedState){//last update was NOT an overspeed
			updateOperators.$set.lastUpdateOverspeedState = true ;
			var newOverspeedEventDoc = {
					"deviceIMEI": newUpdate.deviceIMEI,
					"eventType": "overspeed",
					"eventTime": newUpdate.serverTime,
					"timestamps": [newUpdate.serverTime],
					"locations": [newUpdate.location],
					"speedValues": [newUpdate.speed]
			};
			db.collection('events').insert(newOverspeedEventDoc, function(err, inserted) {
		        if(err) throw err;
		        console.dir("Successfully inserted a new overpeed event: " + JSON.stringify(inserted));
		    });
		}else{
			
			var eventsQuery = { "deviceIMEI": newUpdate.deviceIMEI, "eventType": "overspeed"};
		    var sort = [["eventTime",-1]];
		    var operator = { "$push" : {
		    	"timestamps" : newUpdate.serverTime,
		    	"locations" : newUpdate.location,
		    	"speedValues" : newUpdate.speed
		    } };
		    var options = { 'new' : false };

		    db.collection('events').findAndModify(eventsQuery, sort, operator, options, function(err, doc) {
		        if(err) throw err;
		    });
		}
	}
	else{
		newUpdate.overspeed = false;
		if (doc.lastUpdateOverspeedState==true){
			updateOperators.$set.lastUpdateOverspeedState = false ;
		}
	}
	
	// check if offline flag need to be reset
	if(doc.offlineFlag){// update is the first one just after an offline state
		newUpdate.totalMileage=doc.totalMileage;
		updateOperators.$set.offlineFlag = false;
		
		var eventsQuery = { "deviceIMEI": newUpdate.deviceIMEI, "eventType": "offline"};
	    var sort = [["eventTime",-1]];
	    var operator = { "$set" : {"onlineTime": newUpdate.serverTime, "onlineLocation": newUpdate.location} };
	    var options = { 'new' : false };

	    db.collection('events').findAndModify(eventsQuery, sort, operator, options, function(err, doc) {
	        if(err) throw err;
	    });
	}else{// update is not emerging from an offline state
		var distanceTravelled = gju.pointDistance(doc.lastUpdateLocation, newUpdate.location);// distance to last location in meters
		var newTotalMileage = doc.totalMileage+(distanceTravelled/1000);
		newUpdate.totalMileage = doc.totalMileage+(distanceTravelled/1000);
		updateOperators.$inc.totalMileage = distanceTravelled/1000;
		var eventsQuery = { "deviceIMEI": newUpdate.deviceIMEI, "eventType": "oil change"};
		var options = {sort : [["eventTime",-1]]};
		db.collection('events').findOne(eventsQuery, options, function(err, lastOilAlarm) {
	        if(err) throw err;
	        if(! lastOilAlarm){
	        	if(newTotalMileage > doc.oilMileageThreshold){
	        		var newOilEventDoc = {
	    					"deviceIMEI": newUpdate.deviceIMEI,
	    					"eventType": "oil change",
	    					"eventTime": newUpdate.serverTime,
	    					"totalMileageOnAlarm": newTotalMileage
	    			};
	    			db.collection('events').insert(newOilEventDoc, function(err, inserted) {
	    		        if(err) throw err;
	    		        console.dir("Successfully inserted a new oil change event: " + JSON.stringify(inserted));
	    		    });
	        	}
	        }else{
	        	if(lastOilAlarm.totalMileageOnReset){
	        		if( (newTotalMileage - lastOilAlarm.totalMileageOnReset) > doc.oilMileageThreshold ){
	        			var newOilEventDoc = {
		    					"deviceIMEI": newUpdate.deviceIMEI,
		    					"eventType": "oil change",
		    					"eventTime": newUpdate.serverTime,
		    					"totalMileageOnAlarm": newTotalMileage
		    			};
		    			db.collection('events').insert(newOilEventDoc, function(err, inserted) {
		    		        if(err) throw err;
		    		        console.dir("Successfully inserted a new oil change event: " + JSON.stringify(inserted));
		    		    });
	        		}
	        	}
	        }
	    });
		
	}
	
	// check if a stop event timer need to be stopped
	if(doc.stopDetailTimerFlag){// stop event flag was activated in a previous heartbeat
		updateOperators.$set.stopDetailTimerFlag = false;
		if(!doc.offlineFlag){// device update is not coming from an offline state
			var eventsQuery = { "deviceIMEI": newUpdate.deviceIMEI, "eventType": "stop"};
		    var sort = [["eventTime",-1]];
		    var operator = { "$push" : { "timestamps" : newUpdate.serverTime } };
		    var options = { 'new' : false };

		    db.collection('events').findAndModify(eventsQuery, sort, operator, options, function(err, doc) {
		        if(err) throw err;
		    });
		}
	}
	
	// add new document or update existing document in the 'events' collection in case of a geofence event
	if(typeof doc.geofences !== 'undefined'){
		var geofences = doc.geofences;
		newUpdate.insideGeofence = false;// this default value will change only in case newUpdate.location exists inside a geofence
		for (var i = 0; i < geofences.length; i++) {
			var geofence = geofences[i];
			// var geofenceFlag = gju.pointInPolygon(newUpdate.location,geofence.polygon);
			var geofenceFlag = inside.polygon(geofence.polygon.coordinates, newUpdate.location.coordinates);
			if(geofenceFlag){// point is inside the specified geofence
				newUpdate.insideGeofence = true;
				if(!geofence.lastUpdateGeofenceFlag){// point was NOT inside the specified geofence in the last update
					var newGeofenceEventDoc = {
							"deviceIMEI": newUpdate.deviceIMEI,
							"eventType": "geofence",
							"eventTime": newUpdate.serverTime,
							"geofenceName": geofence.name,
							"geofenceInfo": [{
								"geofenceType": "in",
								"timestamp": newUpdate.serverTime,
								"location": newUpdate.location
							}]
					};
					db.collection('events').insert(newGeofenceEventDoc, function(err, inserted) {
				        if(err) throw err;
				        console.dir("Successfully inserted a new geofence event: " + JSON.stringify(inserted));
				    });
					updateOperators.$set['geofences.'+i+'.lastUpdateGeofenceFlag'] = true;
					updateOperators.$set['geofences.'+i+'.lastGeofenceEventTime'] = newUpdate.serverTime;
					updateOperators.$push.currentGeofencesNames = geofence.name;
				}else{// point was inside the specified geofence in the last update too
					var newGeofenceInfo = {
							"geofenceType": "in",
							"timestamp": newUpdate.serverTime,
							"location": newUpdate.location
					};
					var eventsQuery = {
							"deviceIMEI": newUpdate.deviceIMEI,
							"eventType": "geofence",
							"eventTime": geofence.lastGeofenceEventTime,
							"geofenceName": geofence.name
							
					};
				    var sort = [];
				    var operator = { "$push" : { "geofenceInfo" : newGeofenceInfo } };
				    var options = { 'new' : false };

				    db.collection('events').findAndModify(eventsQuery, sort, operator, options, function(err, doc) {
				        if(err) throw err;
				    });
				}
			}else if(geofence.lastUpdateGeofenceFlag){// point is outside the specified geofence but was inside it in the last update
				updateOperators.$set['geofences.'+i+'.lastUpdateGeofenceFlag'] = false;
				var newGeofenceInfo = {
						"geofenceType": "out",
						"timestamp": newUpdate.serverTime,
						"location": newUpdate.location
				};
				var eventsQuery = {
						"deviceIMEI": newUpdate.deviceIMEI,
						"eventType": "geofence",
						"eventTime": geofence.lastGeofenceEventTime,
						"geofenceName": geofence.name
						
				};
				updateOperators.$pull.currentGeofencesNames = geofence.name;
			    var sort = [];
			    var operator = { "$push" : { "geofenceInfo" : newGeofenceInfo } };
			    var options = { 'new' : false };

			    db.collection('events').findAndModify(eventsQuery, sort, operator, options, function(err, doc) {
			        if(err) throw err;
			    });

			    var newGeofenceEventDoc = {
					"deviceIMEI": newUpdate.deviceIMEI,
					"eventType": "geofence",
					"eventTime": newUpdate.serverTime,
					"geofenceName": geofence.name,
					"geofenceInfo": [{
						"geofenceType": "out",
						"timestamp": newUpdate.serverTime,
						"location": newUpdate.location
					}]
				};
				db.collection('events').insert(newGeofenceEventDoc, function(err, inserted) {
			        if(err) throw err;
			        console.dir("Successfully inserted a new geofence event: " + JSON.stringify(inserted));
			    });

			}
		}
	}
	
	if(miscUtils.isEmpty(updateOperators.$push))delete updateOperators.$push;// delete key if not set
	if(miscUtils.isEmpty(updateOperators.$inc))delete updateOperators.$inc;// delete key if not set
	if(miscUtils.isEmpty(updateOperators.$pull))delete updateOperators.$pull;// delete key if not set
	
	// send the update command to mongodb to update the device's document in the 'devices' collection
	db.collection('devices').update(query, updateOperators, function(err, updated) {
		if(err) {throw err;}
		console.dir("Successfully updated the document of device IMEI " + newUpdate.deviceIMEI);
	});
	
	// Add a new document containing the received information to the updates collection
	db.collection("updates").insert(newUpdate, function(err, inserted) {
		if(err) {throw err;}
		console.dir("Successfully inserted a GPS update to the 'updates' collection for device " + newUpdate.deviceIMEI);
	});
}



/**
 * Update the document of the specified device in the 'devices' collection & add a new GPS update to the 'updates' collection
 * @param {Object} newUpdate containing the following keys of the 'updates' collection & their values:
	 * <br>deviceIMEI: String
	 * <br>updateType: String = gps
	 * <br>deviceTime: Date
	 * <br>serverTime: Date
	 * <br>location: GeoJSON object
	 * <br>speed: 32-bit int
	 * <br>direction: 32-bit int
	 * <br>socketRemoteAddress: String
	 * <br>socketRemotePort: String
	 * <br>packetNumber: 32-bit int
	 * <br>locationAreaCode: 32-bit int
	 * <br>mobileNetworkCode:32-bit int
	 * <br>cellTowerID: 32-bit int
	 * <br>eastLongitude: Boolean
	 * <br>northLatitude: Boolean
	 * <br>gpsLocated: Boolean
 * <br>The remaining keys that need to be set are:
 	 * <br>overspeed
	 * <br>totalMileage
	 * <br>insideGeofence.
 * @param {Db} db : mongodb database handle
 */
function handleGPSUpdate(newUpdate, db){
	var query = { "_id" : newUpdate.deviceIMEI };
	var projection = {
			'_id' : 0,
			'overspeed' : 1,
			'geofences' : 1,
			'totalMileage' : 1,
			'lastUpdateOverspeedState' : 1,
			'lastUpdateLocation' : 1,
			'offlineFlag' : 1,
			'stopDetailTimerFlag' : 1,
			'oilMileageThreshold': 1,
			'tyreMileageThreshold': 1,
			'brakeMileageThreshold': 1
			};
	
	db.collection("devices").findOne(query, projection, function(err, doc) {
		if(err) {throw err;}			
		if(!doc) {
			console.log('GPS handle: No documents for device IMEI ' + newUpdate.deviceIMEI + ' was found in the database!');
			createNewDevice(newUpdate, db);
		}else{
			checkEventsAndUpdate(newUpdate, doc, db);
		}
	});
}



/**
* Resolve the address of the given location & create hexastring message that will be sent to terminal
 * @param {Object} location GeoJSON object representing the location that needs to be resolved to an address
 * @param {String} phoneNumHex a hexadecimal string representation of the device's phone number
 * @returns {String} addressResponse message that will be sent to terminal in 'Hex' encoding
 */
function createAndSendAddressResponse(location,phoneNumHex,packetNumber,socket){	
	geocoder.reverseGeocode( location.coordinates[0], location.coordinates[1], function ( err, data ) {
		if(err) {throw err;}
		
		if(typeof data.results[0].formatted_address !== 'undefined'){
			var command = '11000011';
			command += miscUtils.asciiString2Hex("ADDRESS&&"+addressContentString+"&&");
			command += phoneNumHex+miscUtils.asciiString2Hex("##");
			
			var commandByteLength = command.length/2;
			var dataLength = commandByteLength + 7;
			
			commandByteLength = commandByteLength.toString(16);
			while (commandByteLength.length < 4) { commandByteLength = '0' + commandByteLength; } // Zero pad.
			
			dataLength = dataLength.toString(16);
			while (dataLength.length < 4) { dataLength = '0' + dataLength; } // Zero pad.
			
			packetNumber = packetNumber.toString(16);
			while (packetNumber.length < 4) { packetNumber = '0' + packetNumber; } // Zero pad.
			
			var crcData = dataLength+'97'+commandByteLength+command+packetNumber;
			var crcHexStr = miscUtils.crctab16(crcData);
			
			var addressResponse = '7878'+crcData+crcHexStr+'0D0A';
			console.log("Address response packet: "+ addressResponse +" will be sent to terminal");
			socket.write(addressResponse, 'hex', function(){
				console.log("Address response packet: "+ addressResponse +" was sent to terminal");
			});
		}else{
			
			var addressContentString = 'No address was found'; // default value
			
			var command = '11000011';
			command += miscUtils.asciiString2Hex("ADDRESS&&"+addressContentString+"&&");
			command += phoneNumHex+miscUtils.asciiString2Hex("##");
			
			var commandByteLength = command.length/2;
			var dataLength = commandByteLength + 7;
			
			commandByteLength = commandByteLength.toString(16);
			while (commandByteLength.length < 4) { commandByteLength = '0' + commandByteLength; } // Zero pad.
			
			dataLength = dataLength.toString(16);
			while (dataLength.length < 4) { dataLength = '0' + dataLength; } // Zero pad.
			
			packetNumber = packetNumber.toString(16);
			while (packetNumber.length < 4) { packetNumber = '0' + packetNumber; } // Zero pad.
			
			var crcData = dataLength+'97'+commandByteLength+command+packetNumber;
			var crcHexStr = miscUtils.crctab16(crcData);
			
			var addressResponse = '7878'+crcData+crcHexStr+'0D0A';
			console.log("Address response packet: "+ addressResponse +" will be sent to terminal");
			socket.write(addressResponse, 'hex', function(){
				console.log("Address response packet: "+ addressResponse +" was sent to terminal");
			});
		}
	});
}


/**
 * Add a new Address update to the updates collection
 * @param {Object} newUpdate containing the following keys of the updates collection & their values:
	 * <br>deviceIMEI: String
	 * <br>updateType: String = address
	 * <br>deviceTime: Date
	 * <br>serverTime: Date
	 * <br>location: GeoJSON object
	 * <br>speed: 32-bit int
	 * <br>direction: 32-bit int
	 * <br>socketRemoteAddress: String
	 * <br>socketRemotePort: String
	 * <br>packetNumber: 32-bit int
	 * <br>locationAreaCode: 32-bit int
	 * <br>mobileNetworkCode:32-bit int
	 * <br>cellTowerID: 32-bit int
	 * <br>eastLongitude: Boolean
	 * <br>northLatitude: Boolean
	 * <br>gpsLocated: Boolean
	 * <br>phoneNumber: String
	 * <br>Language: 32-bit int = 01 for chinese or 02 for English
 * @param {Db} db : mongodb database handle
 */
function handleAddressUpdate(newUpdate, db){
	// Add a new document containing the received information to the updates collection
	db.collection("updates").insert(newUpdate, function(err, inserted) {
		if(err) {
			throw err;
		}
		console.dir("Successfully inserted an Address update to the 'updates' collection for device " + newUpdate.deviceIMEI);
	});
}

/**
 * Reset timers & flags in the document of the specified device in the 'devices' collection and add offline event to the
 * 'events' collection & check for a running stop event to add a final timestamp to it
 * @param {String} deviceIMEI IMEI number for the device whose connection has closed
 * @param {Db} db : mongodb database handle
 */
function handleClosedConnection(deviceIMEI, db){
	var query = { "_id" : deviceIMEI };
	var projection = { '_id' : 0, 'lastUpdateLocation' : 1 };
	
	db.collection("devices").findOne(query, projection, function(err, doc) {
		if(err) {throw err;}
		
		if(!doc) {
			console.log('Closed conn handle: No documents for device IMEI ' + deviceIMEI + ' was found in the database!');
		}else{
			var updateOperators = { "$set" : { "offlineFlag" : true, "lastUpdateOverspeedState" : false, "stopDetailTimerFlag": false} };
			var serverTime = new Date();
			
			var newOfflineEventDoc = {
					"deviceIMEI": deviceIMEI,
					"eventType": "offline",
					"eventTime": serverTime,
					"offlineLocation": doc.lastUpdateLocation
			};
			db.collection('events').insert(newOfflineEventDoc, function(err, inserted) {
		        if(err) throw err;
		        console.dir("Successfully inserted a new offline event: " + JSON.stringify(inserted));
		    });
			
			// check if a stop event timer need to be stopped & add a final timestamp to it
			if(doc.stopDetailTimerFlag){// stop event flag was activated in a previous heartbeat
				var eventsQuery = { "deviceIMEI": deviceIMEI, "eventType": "stop"};
			    var sort = [["eventTime",-1]];
			    var operator = { "$push" : { "timestamps" : newUpdate.serverTime } , "$set": {"nextEvent": "offline"}};
			    var options = { 'new' : false };

			    db.collection('events').findAndModify(eventsQuery, sort, operator, options, function(err, doc) {
			        if(err) throw err;
			    });
			}
			
			db.collection('devices').update(query, updateOperators, function(err, updated) {
				if(err) {throw err;}
				console.dir("Successfully updated the document of device IMEI " + deviceIMEI);
			});
		}
	});
}

MongoClient.connect(mongodServer, function(err, db) { // single mongodb db connection for all devices
	// default poolsize=5 so in reality the number of concurrent connections is 5 not 1
	// if queries & writes turn out to be running slow check this for troubleshooting: http://bit.ly/1suZ9oM
	if(err) {
		throw err;
	}else{
		console.log('connected to db');
		const amqp = require('amqplib');
		amqp.connect({
			protocol: 'amqp',
			hostname: 'rabbitmq-traccar',
			port: 5672
		}).then((conn) => {
			return conn.createConfirmChannel();
		}).then((ch) => {
			ch.assertQueue('position', {
				durable: true
			}).then((pq) => {
				ch.consume(pq.queue, (msg) => {
					const newUpdate = JSON.parse(msg.content.toString());
					if (newUpdate.deviceTime) {
						newUpdate.deviceTime = new Date(newUpdate.deviceTime + "Z");
					}
					newUpdate.serverTime = new Date();
					console.log(`Received position update ${newUpdate} on rabbitMQ`)
					ch.ack(msg);
					handleGPSUpdate(newUpdate, db);
				});				
				console.log('rabbitMQ position queue asserted')
			});

			ch.assertQueue('disconnected', {
				durable: true
			}).then((dq) => {
				ch.consume(dq.queue, (msg) => {
					const deviceIMEI = msg.content.toString();
					console.log(`Received disconnected event for device# ${deviceIMEI} on rabbitMQ`)
					ch.ack(msg);
					handleClosedConnection(deviceIMEI, db);
				});
				console.log('rabbitMQ disconnected queue asserted')
			});
		});
		// Start a TCP Server
		net.createServer(function (socket) {
			// Identify this device
			socket.name = socket.remoteAddress + ":" + socket.remotePort;
			
			var connectionInfoAdded = false;
			
			
					
			// Handle incoming messages from devices.
			socket.on('data', function (data){
				//var inMessage = data.toString('hex');// input message from device is received in Hexadecimal encoding
				var inMessage = data.toString();// input message from virtual device is received in Hexadecimal encoding
				
				var deviceIMEI = inMessage.slice(10,26);
				
				if(!connectionInfoAdded){
					var connectionInfo = new connection(socket, socket.name, socket.remoteAddress, socket.remotePort, deviceIMEI)
					// Put this new device connection in the list
					devicesConnections.push(connectionInfo);
					console.dir(connectionInfo);
					connectionInfoAdded = true;
				}
				
				
				var protocolNum = inMessage.slice(30,32);
				console.log("--------------------------");
				console.log(socket.name + " with IMEI " + deviceIMEI + " on protocol " + protocolNum + " > \n" + inMessage);
				
				
				switch (protocolNum.toLowerCase()) {
				case '1a': //Heartbeat packet
					socket.write('54681A0D0A', 'hex', function(){
						console.log("Heartbeat response packet: 54681A0D0A was sent to terminal");
					});
					var newUpdate = parseHeartbeatMessage(inMessage, socket);
					
					var stopDetailThreshold = 60*1000;
					handleHearbeatUpdate(newUpdate, stopDetailThreshold, db);
					break;
					
				case '10': //GPS data packet
					
					var newUpdate = parseGpsOrAddressMessage('gps',inMessage, socket);
					console.log(newUpdate);
					console.log("Date & time: " + newUpdate.deviceTime + "Speed: "+newUpdate.speed+" Course: "+newUpdate.direction);
					console.log("Location: " + JSON.stringify(newUpdate.location));
					handleGPSUpdate(newUpdate, db);
					break;
					
				case '1b': //address request packet
					var newUpdate = parseGpsOrAddressMessage('address',inMessage, socket);
					var phoneNumHex = inMessage.slice(80,122);
					newUpdate.phoneNum = miscUtils.hexString2Ascii(phoneNumHex);
					newUpdate.language = inMessage.slice(122,124);
					
					createAndSendAddressResponse(newUpdate.location,phoneNumHex,newUpdate.packetNumber,socket);
					handleAddressUpdate(newUpdate, db);
					break;
		
				default:
					console.log("No cases were hit");
					break;
				}
			});
			
			
			// Remove the device from the list when it leaves & stop any open timers or flags
			socket.on('close', function () {// 'end' event might be replaced by 'close' event [need testing]
				console.log('Closed: ' + socket.name);
				
				var socketIndex;
				var deviceIMEI;
				
				for (var i = 0; i < devicesConnections.length; i++) {
					var connection = devicesConnections[i];
					if(connection.socket == socket){
						socketIndex = i;
						deviceIMEI = connection.deviceIMEI;
						break;
					}
				}
				
				if(typeof deviceIMEI !== 'undefined'){
					devicesConnections.splice(socketIndex, 1);
					handleClosedConnection(deviceIMEI, db);
				}
				
			});
			
			socket.on('error', function(err){
				console.log(err);
		    });
				
		 
		}).listen(portNum);
	} // else
}); // MongoClient.connect
 
// Put a friendly message on the terminal of the server.
console.log("Server running at port "+portNum+"\n");






