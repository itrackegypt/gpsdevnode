//Load the TCP Library
var net = require('net');
//Load the GeoJSON Utilities for JavaScript
var gju = require('geojson-utils');
//Load library that resolves location into address
var geocoder = require('geocoder');

var Utilities = require('./utilities');
var miscUtils = new Utilities();
// Load mongodb library
var MongoClient = require('mongodb').MongoClient;

// mongod/mongos server location & port
var mongodServer = 'mongodb://localhost:27017/iTrackEgypt';

// server listening port
var portNum = 3000;

// creating an object to hold connection information for a connected device
function connection(socket, name, remoteAddress, remotePort, deviceIMEI) {
    this.socket = socket;
    this.name = name;
    this.remoteAddress = remoteAddress;
    this.remotePort = remotePort;
    this.deviceIMEI = deviceIMEI;
}

// Keep track of the connection information of each connected device 
var devicesConnections = [];// an array of 'connection' data type

/**
 * Takes in the Heartbeat message and returns a document with most of the keys of the 'updates' collection & their
 * corresponding values
 * @param {String} inMessage : the message sent by the device in Hexadecimal encoding
 * @returns: {Object} newUpdate : a key value pair containing the following keys of the 'updates' collection & their
 * corresponding values:
	 * <br>deviceIMEI: String
	 * <br>updateType: String = heartbeat
	 * <br>serverTime: Date
	 * <br>voltageLevel: 32-bit int
	 * <br>gsmSignal: 32-bit int
	 * <br>packetNumber: 32-bit int
	 * <br>positioningState: 32-bit int
	 * <br>satellitesNumber: 32-bit int
	 * <br>satellitesSNR: Array of 32-bit int
 * @note: the remaining key that needs to be set is: stopFlag
 */
function parseHeartbeatMessage(inMessage, socket){
	var newUpdate = new Object();
	
	newUpdate.socketRemoteAddress = socket.remoteAddress;
	newUpdate.socketRemotePort = socket.remotePort;
	
	newUpdate.updateType = "heartbeat";
	newUpdate.serverTime = new Date();
	
	var packetLength = parseInt(inMessage.slice(4,6),16);
	newUpdate.voltageLevel = parseInt(inMessage.slice(6,8),16);
	newUpdate.gsmSignal = parseInt(inMessage.slice(8,10),16);
	newUpdate.deviceIMEI = inMessage.slice(10,26);
	newUpdate.packetNumber = parseInt(inMessage.slice(26,30),16);
	newUpdate.positioningState = parseInt(inMessage.slice(32,34),16);
	newUpdate.satellitesNumber = parseInt(inMessage.slice(34,36),16);
	newUpdate.satellitesSNR = [];
	var start = 36
	for (var i = 0; i < newUpdate.satellitesNumber; i++) {
		var satelliteSNR = parseInt(inMessage.slice(start,start+2),16);
		start = start + 2;
		newUpdate.satellitesSNR.push(satelliteSNR);
	}
	
	return newUpdate;
}

/**
 * Update the document of the specified device in the 'devices' collection & add a new Heartbeat update to the 'updates' collection
 * @param {String} deviceIMEI : IMEI number of the device to check on {Number} stopDetailThreshold : the minimum
 * time in milliseconds to trigger a stop event (time between this heartbeat and the last update)
 */
function handleHearbeatUpdate(newUpdate, stopDetailThreshold){
	MongoClient.connect(mongodServer, function(err, db) {
		if(err) {
			throw err;
		}
		
		var projection = {
				'_id' : 0,
				'lastUpdateTime' : 1,
				'stopDetailTimerFlag' : 1,
				'stopDetailsArraySize' : 1,
				'lastUpdateLocation' : 1
				};
		
		db.collection("devices").findOne({ "_id" : newUpdate.deviceIMEI }, function(err, doc) {
			if(err) {
				throw err;
			}
			
			if(!doc) { // document doesn't exist 
				console.log('Heartbeat handle: No documents for device IMEI ' + newUpdate.deviceIMEI + ' was found in the database!');
				//return db.close();
			}else{ // document exists
				var query = { "_id" : newUpdate.deviceIMEI };
				var updateOperators = {
						'$set' : { "lastHeartbeat" : newUpdate.serverTime},// update 'lastHeartbeat' key in the device's document
						"$push" : {},
						"$inc" : {}
				};
				
				// Add new object or update existing object in 'stopDetails' array in case of a stop event
				if (!doc.offlineFlag && newUpdate.serverTime.getTime() - doc.lastUpdateTime.getTime() > stopDetailThreshold ){// Check for a 'stop' event
					newUpdate.stopFlag = true;
					if (!doc.stopDetailTimerFlag){// stop event flag wasn't activated before this Heartbeat
						updateOperators.$set.stopDetailTimerFlag = true ;
						var stopDetail = {"timestamps": [newUpdate.serverTime], "location": doc.lastUpdateLocation};
						updateOperators.$push.stopDetails = stopDetail;
						updateOperators.$inc.stopDetailsArraySize = 1;
					}else{// stop event flag was activated in a previous heartbeat
						var index = doc.stopDetailsArraySize - 1;
						updateOperators.$push['stopDetails.'+index+'.timestamps'] = newUpdate.serverTime;
					}
				}
				
				if(miscUtils.isEmpty(updateOperators.$push))delete updateOperators.$push;// delete key if not set
				if(miscUtils.isEmpty(updateOperators.$inc))delete updateOperators.$inc;// delete key if not set
				
				db.collection('devices').update(query, updateOperators, function(err, updated) {
					if(err) {
						throw err;
					}
					console.dir("Successfully updated the document of device IMEI " + newUpdate.deviceIMEI);
					//return db.close();;
				});
				
				// Add a new document containing the received information to the updates collection
				db.collection("updates").insert(newUpdate, function(err, inserted) {
					if(err) {
						throw err;
					}
					console.dir("Successfully inserted a Heartbeat update to the 'updates' collection for device " + newUpdate.deviceIMEI);
				});
				
			}
		});
		
		
	});
}

/**
 * Takes in the GPS data message and returns a document with most of the keys of the 'updates' collection & their
 * corresponding values
 * @param {String} inMessage : the message sent by the device in Hexadecimal encoding
 * @returns: {Object} newUpdate : a key value pair containing the following keys of the 'updates' collection & their
 * corresponding values:
	 * <br>deviceIMEI: String
	 * <br>updateType: String = gps
	 * <br>deviceTime: Date
	 * <br>serverTime: Date
	 * <br>location: GeoJSON object
	 * <br>speed: 32-bit int
	 * <br>direction: 32-bit int
	 * <br>packetNumber: 32-bit int
	 * <br>locationAreaCode: 32-bit int
	 * <br>mobileNetworkCode:32-bit int
	 * <br>cellTowerID: 32-bit int
	 * <br>eastLongitude: Boolean
	 * <br>northLatitude: Boolean
	 * <br>gpsLocated: Boolean
 * @note: the remaining keys that need to be set are: overspeed, totalMileage and insideGeofence.
 */
function parseGpsOrAddressMessage(str,inMessage,socket){
	var newUpdate = new Object();
	
	newUpdate.socketRemoteAddress = socket.remoteAddress;
	newUpdate.socketRemotePort = socket.remotePort;
	
	newUpdate.updateType = str;
	newUpdate.serverTime = new Date();
	
	newUpdate.locationAreaCode = parseInt(inMessage.slice(6,10),16);
	newUpdate.deviceIMEI = inMessage.slice(10,26);
	newUpdate.packetNumber = parseInt(inMessage.slice(26,30),16);
	//newUpdate.protocolNum = inMessage.slice(30,32);
	var dateTimeHex = inMessage.slice(32,44);
	var yearDec = parseInt(dateTimeHex.slice(0,2),16);
	var yearString = yearDec<10? '0'+yearDec : yearDec;
	var monthDec = parseInt(dateTimeHex.slice(2,4),16);
	var monthString = monthDec<10? '0'+monthDec : monthDec;
	var dayDec = parseInt(dateTimeHex.slice(4,6),16);
	var dayString = dayDec<10? '0'+dayDec : dayDec;
	var hourDec = parseInt(dateTimeHex.slice(6,8),16);
	var hourString = hourDec<10? '0'+hourDec : hourDec;
	var minutesDec = parseInt(dateTimeHex.slice(8,10),16);
	var minutesString = minutesDec<10? '0'+minutesDec : minutesDec;
	var secondsDec = parseInt(dateTimeHex.slice(-2),16);
	var secondsString = secondsDec<10? '0'+secondsDec : secondsDec;
	var dateTimeString = '20'+yearString+'-'+monthString+'-'+dayString+'T'+hourString+':'+minutesString+':'+secondsString+'.000Z';
	newUpdate.deviceTime = new Date(dateTimeString.toString());
	var latitudeHex = inMessage.slice(44,52);
	var latitudeDec = parseInt(latitudeHex,16)/1800000;
	var longitudeHex = inMessage.slice(52,60);
	var longitudeDec = parseInt(longitudeHex,16)/1800000;
	newUpdate.location = { type: "Point", coordinates: [ latitudeDec, longitudeDec ] };
	var speedHex = inMessage.slice(60,62);
	newUpdate.speed = parseInt(speedHex,16);
	var directionHex = inMessage.slice(62,66);
	newUpdate.direction = parseInt(directionHex,16) & 1023;
	newUpdate.mobileNetworkCode = parseInt(inMessage.slice(66,68),16);
	newUpdate.cellTowerID = parseInt(inMessage.slice(68,72),16);
	var statusInfo = inMessage.slice(78,80);
	newUpdate.eastLongitude = (parseInt(statusInfo,16) & 4)? true:false;
	newUpdate.northLatitude = (parseInt(statusInfo,16) & 2)? true:false;
	newUpdate.gpsLocated = (parseInt(statusInfo,16) & 1)? true:false;
	
	return newUpdate;
}

/**
 * Creates a new document representing a device in the 'devices' collection while adding a new update to the
 * 'updates' collection
 * @param newUpdate containing most of the keys of the 'updates' collection & their values except the
 * following keys that need to be set:
	 * <br>overspeed
	 * <br>totalMileage
	 * <br>insideGeofence.
 */
function createNewDevice(newUpdate){
	MongoClient.connect(mongodServer, function(err, db) {
		if(err) {
			throw err;
		}
		var device = {_id:newUpdate.deviceIMEI};
    	device.type = 'TR02';
    	device.fuelConsumption100KM = 20;
    	device.overspeed = 80;
    	
    	device.totalMileage = 0.0;
    	device.offlineFlag = false;
    	//device.stopDetailTimerFlag = false;
    	device.geofenceAlarmsArraySize = 0;
    	device.lastUpdateTime = newUpdate.serverTime;
    	device.lastUpdateLocation = newUpdate.location;
		device.lastUpdateSpeed = newUpdate.speed;
		device.lastUpdateDirection = newUpdate.direction;
		
		// add new object or update existing object in 'overspeedDetails' array in case of an overspeed event
		if(newUpdate.speed>device.overspeed){
			newUpdate.overspeed = true;
			device.lastUpdateOverspeedState = true ;
			var overspeedDetail = {
					"timestamps": [newUpdate.serverTime],
					"locations": [newUpdate.location],
					"speedValues": [newUpdate.speed]
			};
			device.overspeedDetails = [overspeedDetail];
			device.overspeedDetailsArraySize  = 1;
		}else{
			newUpdate.overspeed = false;
		}
		
		newUpdate.totalMileage=device.totalMileage;
		
		// Insert new device to the 'devices' collection
		db.collection('devices').insert(device, function(err, inserted) {
	        if(err) throw err;
	        console.dir("Successfully inserted new device: " + JSON.stringify(inserted));
	    });
		
		// Add a new document containing the received information to the updates collection
		db.collection("updates").insert(newUpdate, function(err, inserted) {
			if(err) {
				throw err;
			}
			console.dir("Successfully inserted an update to the 'updates' collection for device " + newUpdate.deviceIMEI);
		});
		
		
	});
}

/**
 * Uses the GPS update to check for an overspeed event, an offline event, a stop event or a geofence event then modifies
 * the keys representing those events in the database 
 * @param {Object} newUpdate containing most of the keys of the 'updates' collection & their values except the
 * following keys that need to be set:
	 * <br>overspeed
	 * <br>totalMileage
	 * <br>insideGeofence.
 * @param {Object} doc : The document of the specified device in the 'devices' collection
 * @returns {Object} updateOperators : the keys that need to be updated in the document of the specified device
 */
function checkGPSUpdate(newUpdate, doc){
	
	var updateOperators = {
			"$set" : { "lastUpdateTime" : newUpdate.serverTime , "lastUpdateLocation" : newUpdate.location },
			"$push" : {},
			"$inc" : {},
			"$pull" : {}
	};
	
	// add new object or update existing object in 'overspeedDetails' array in case of an overspeed event
	if(newUpdate.speed>doc.overspeed){
		newUpdate.overspeed = true;
		if (!doc.lastUpdateOverspeedState){//last update was NOT an overspeed
			updateOperators.$set.lastUpdateOverspeedState = true ;
			var overspeedDetail = {
					"timestamps": [newUpdate.serverTime],
					"locations": [newUpdate.location],
					"speedValues": [newUpdate.speed]
			};
			updateOperators.$push.overspeedDetails = overspeedDetail;
			updateOperators.$inc.overspeedDetailsArraySize  = 1;
		}else{
			var index = doc.overspeedDetailsArraySize  - 1;
			updateOperators.$push['overspeedDetails.'+index+'.timestamps'] = newUpdate.serverTime;
			updateOperators.$push['overspeedDetails.'+index+'.locations'] = newUpdate.location;
			updateOperators.$push['overspeedDetails.'+index+'.speedValues'] = newUpdate.speed;
		}
	}
	else{
		newUpdate.overspeed = false;
		if (doc.lastUpdateOverspeedState==true){
			updateOperators.$set.lastUpdateOverspeedState = false ;
		}
	}
	
	// check if offline flag need to be reset
	if(doc.offlineFlag){// update is the first one just after an offline state
		newUpdate.totalMileage=doc.totalMileage;
		//updateOperators.$inc.totalMileage = newUpdate.totalMileage;
		updateOperators.$set.offlineFlag = false;
		var index = doc.offlineAlarmsArraySize - 1
		updateOperators.$set['offlineAlarms.'+index+'.onlineTime'] = newUpdate.serverTime;
		updateOperators.$set['offlineAlarms.'+index+'.onlineLocation'] = newUpdate.location;
	}else{// update is not emerging from an offline state
		var distanceTravelled = gju.pointDistance(doc.lastUpdateLocation, newUpdate.location);// distance to last location in meters
		newUpdate.totalMileage = doc.totalMileage+(distanceTravelled/1000);
		updateOperators.$inc.totalMileage = distanceTravelled/1000;
	}
	
	// check if a stop event timer need to be stopped
	if(doc.stopDetailTimerFlag){// stop event flag was activated in a previous heartbeat
		updateOperators.$set.stopDetailTimerFlag = false;
		if(!doc.offlineFlag){// device update is not coming from an offline state
			var index = doc.stopDetailsArraySize - 1;
			updateOperators.$push['stopDetails.'+index+'.timestamps'] = newUpdate.serverTime;
		}
	}
	
	// add new object or update existing object in 'geofenceAlarms' array in case of a geofence event
	if(typeof doc.geofences !== 'undefined'){
		var geofences = doc.geofences;
		newUpdate.insideGeofence = false;// this default value will change only in case newUpdate.location exists inside a geofence
		var newGeofenceAlarmsAdded = 0;
		for (var i = 0; i < geofences.length; i++) {
			var geofence = geofences[i];
			var geofenceFlag = gju.pointInPolygon(newUpdate.location,geofence.polygon);
			if(geofenceFlag){// point is inside the specified geofence
				newUpdate.insideGeofence = true;
				if(!geofence.lastUpdateGeofenceFlag){// point was NOT inside the specified geofence in the last update
					var geofenceAlarm = {// creating a new geofenceAlarm object to be added to the 'geofenceAlarms' array
							"geofenceName": geofence.name,
							"geofenceInfo": [{
								"geofenceType": "in",
								"timestamp": newUpdate.serverTime,
								"location": newUpdate.location
							}]
					
					};
					updateOperators.$push.geofenceAlarms = geofenceAlarm;
					updateOperators.$set['geofences.'+i+'.lastUpdateGeofenceFlag'] = true;
					updateOperators.$set['geofences.'+i+'.indexOfCurrentGeofenceAlarm'] = doc.geofenceAlarmsArraySize + newGeofenceAlarmsAdded;
					updateOperators.$push.indexesOfCurrentGeofences = doc.geofenceAlarmsArraySize + newGeofenceAlarmsAdded++;
				}else{// point was inside the specified geofence in the last update too
					var index = geofence.indexOfCurrentGeofenceAlarm;
					var newGeofenceInfo = {
							"geofenceType": "in",
							"timestamp": newUpdate.serverTime,
							"location": newUpdate.location
					};
					updateOperators.$push['geofenceAlarms.'+index+'.geofenceInfo'] = newGeofenceInfo;
				}
			}else if(geofence.lastUpdateGeofenceFlag){// point is outside the specified geofence but was inside it in the last update
				var index = geofence.indexOfCurrentGeofenceAlarm;
				var newGeofenceInfo = {
						"geofenceType": "out",
						"timestamp": newUpdate.serverTime,
						"location": newUpdate.location
				};
				updateOperators.$push['geofenceAlarms.'+index+'.geofenceInfo'] = newGeofenceInfo;
				updateOperators.$set['geofences.'+i+'.lastUpdateGeofenceFlag'] = false;
				updateOperators.$pull.indexesOfCurrentGeofences = index;
			}
		}
		updateOperators.$inc.geofenceAlarmsArraySize = newGeofenceAlarmsAdded;
	}
	
	if(miscUtils.isEmpty(updateOperators.$push))delete updateOperators.$push;// delete key if not set
	if(miscUtils.isEmpty(updateOperators.$inc))delete updateOperators.$inc;// delete key if not set
	if(miscUtils.isEmpty(updateOperators.$pull))delete updateOperators.$pull;// delete key if not set
	
	return updateOperators;
}

/**
 * Update the document of the specified device in the 'devices' collection & add a new GPS update to the 'updates' collection
 * @param {Object} newUpdate containing the following keys of the 'updates' collection & their values:
	 * <br>deviceIMEI: String
	 * <br>updateType: String = gps
	 * <br>deviceTime: Date
	 * <br>serverTime: Date
	 * <br>location: GeoJSON object
	 * <br>speed: 32-bit int
	 * <br>direction: 32-bit int
	 * <br>socketRemoteAddress: String
	 * <br>socketRemotePort: String
	 * <br>packetNumber: 32-bit int
	 * <br>locationAreaCode: 32-bit int
	 * <br>mobileNetworkCode:32-bit int
	 * <br>cellTowerID: 32-bit int
	 * <br>eastLongitude: Boolean
	 * <br>northLatitude: Boolean
	 * <br>gpsLocated: Boolean
 * <br>The remaining keys that need to be set are:
	 * <br>overspeed
	 * <br>totalMileage
	 * <br>insideGeofence.
 */
function handleGPSUpdate(newUpdate){
	MongoClient.connect(mongodServer, function(err, db) {
		if(err) {
			throw err;
		}
		
		var query = { "_id" : newUpdate.deviceIMEI };
		var projection = {
				'_id' : 0,
				'overspeed' : 1,
				'geofences' : 1,
				'totalMileage' : 1,
				'lastUpdateOverspeedState' : 1,
				'overspeedDetailsArraySize' : 1,
				'lastUpdateLocation' : 1,
				'lastUpdateTime' : 1,
				'offlineFlag' : 1,
				'stopDetailTimerFlag' : 1,
				'stopDetailsArraySize' : 1,
				'offlineAlarmsArraySize' : 1,
				'geofenceAlarmsArraySize' : 1
				};
		
		db.collection("devices").findOne(query, projection, function(err, doc) {
			if(err) {
				throw err;
			}
			
			if(!doc) {
				console.log('GPS handle: No documents for device IMEI ' + newUpdate.deviceIMEI + ' was found in the database!');
				createNewDevice(newUpdate);
				//return db.close();
				//insertNewDevice();
			}else{
				/////////////////////////////////
				var updateOperators = checkGPSUpdate(newUpdate, doc);
				//////////////////////////////////
				
				// send the update command to mongodb to update the device's document in the 'devices' collection
				db.collection('devices').update(query, updateOperators, function(err, updated) {
					if(err) {
						throw err;
					}
					console.dir("Successfully updated the document of device IMEI " + newUpdate.deviceIMEI);
				});
				
				// Add a new document containing the received information to the updates collection
				db.collection("updates").insert(newUpdate, function(err, inserted) {
					if(err) {
						throw err;
					}
					console.dir("Successfully inserted a GPS update to the 'updates' collection for device " + newUpdate.deviceIMEI);
				});
			}
			
		});
		
	});
}

/**
 * Resolve the address of the given location & create hexastring message that will be sent to terminal
 * @param {Object} location GeoJSON object representing the location that needs to be resolved to an address
 * @param {String} phoneNumHex a hexadecimal string representation of the device's phone number
 * @returns {String} addressResponse message that will be sent to terminal in 'Hex' encoding
 */
function createAndSendAddressResponse(location,phoneNumHex,packetNumber,socket){
	
	geocoder.reverseGeocode( location.coordinates[0], location.coordinates[1], function ( err, data ) {
		if(err) {
			throw err;
		}
		
		if(typeof data.results[0].formatted_address !== 'undefined'){
			var command = '11000011';
			command += miscUtils.asciiString2Hex("ADDRESS&&"+addressContentString+"&&");
			command += phoneNumHex+miscUtils.asciiString2Hex("##");
			
			var commandByteLength = command.length/2;
			var dataLength = commandByteLength + 7;
			
			commandByteLength = commandByteLength.toString(16);
			while (commandByteLength.length < 4) { commandByteLength = '0' + commandByteLength; } // Zero pad.
			
			dataLength = dataLength.toString(16);
			while (dataLength.length < 4) { dataLength = '0' + dataLength; } // Zero pad.
			
			packetNumber = packetNumber.toString(16);
			while (packetNumber.length < 4) { packetNumber = '0' + packetNumber; } // Zero pad.
			
			var crcData = dataLength+'97'+commandByteLength+command+packetNumber;
			var crcHexStr = miscUtils.crctab16(crcData);
			
			var addressResponse = '7878'+crcData+crcHexStr+'0D0A';
			console.log("Address response packet: "+ addressResponse +" will be sent to terminal");
			socket.write(addressResponse, 'hex', function(){
				console.log("Address response packet: "+ addressResponse +" was sent to terminal");
			});
		}else{
			
			var addressContentString = 'No address was found'; // default value
			
			var command = '11000011';
			command += miscUtils.asciiString2Hex("ADDRESS&&"+addressContentString+"&&");
			command += phoneNumHex+miscUtils.asciiString2Hex("##");
			
			var commandByteLength = command.length/2;
			var dataLength = commandByteLength + 7;
			
			commandByteLength = commandByteLength.toString(16);
			while (commandByteLength.length < 4) { commandByteLength = '0' + commandByteLength; } // Zero pad.
			
			dataLength = dataLength.toString(16);
			while (dataLength.length < 4) { dataLength = '0' + dataLength; } // Zero pad.
			
			packetNumber = packetNumber.toString(16);
			while (packetNumber.length < 4) { packetNumber = '0' + packetNumber; } // Zero pad.
			
			var crcData = dataLength+'97'+commandByteLength+command+packetNumber;
			var crcHexStr = miscUtils.crctab16(crcData);
			
			var addressResponse = '7878'+crcData+crcHexStr+'0D0A';
			console.log("Address response packet: "+ addressResponse +" will be sent to terminal");
			socket.write(addressResponse, 'hex', function(){
				console.log("Address response packet: "+ addressResponse +" was sent to terminal");
			});
		}
	});
	
	
}

/**
 * Add a new Address update to the updates collection
 * @param {Object} newUpdate containing the following keys of the updates collection & their values:
	 * <br>deviceIMEI: String
	 * <br>updateType: String = address
	 * <br>deviceTime: Date
	 * <br>serverTime: Date
	 * <br>location: GeoJSON object
	 * <br>speed: 32-bit int
	 * <br>direction: 32-bit int
	 * <br>socketRemoteAddress: String
	 * <br>socketRemotePort: String
	 * <br>packetNumber: 32-bit int
	 * <br>locationAreaCode: 32-bit int
	 * <br>mobileNetworkCode:32-bit int
	 * <br>cellTowerID: 32-bit int
	 * <br>eastLongitude: Boolean
	 * <br>northLatitude: Boolean
	 * <br>gpsLocated: Boolean
	 * <br>phoneNumber: String
	 * <br>Language: 32-bit int = 01 for chinese or 02 for English
 */
function handleAddressUpdate(newUpdate){
	MongoClient.connect(mongodServer, function(err, db) {
		if(err) {
			throw err;
		}	
				
		// Add a new document containing the received information to the updates collection
		db.collection("updates").insert(newUpdate, function(err, inserted) {
			if(err) {
				throw err;
			}
			console.dir("Successfully inserted an Address update to the 'updates' collection for device " + newUpdate.deviceIMEI);
		});
		
	});
}

/**
 * Reset timers & flags in the document of the specified device in the 'devices' collection
 * @param {String} deviceIMEI IMEI number for the device whose connection has closed
 */
function handleClosedConnection(deviceIMEI){
	
	MongoClient.connect(mongodServer, function(err, db) {
		if(err) {
			throw err;
		}
		
		var query = { "_id" : deviceIMEI };
		var projection = {
				'_id' : 0,
				'lastUpdateLocation' : 1
				};
		
		db.collection("devices").findOne(query, projection, function(err, doc) {
			if(err) {
				throw err;
			}
			
			if(!doc) {
				console.log('Closed conn handle: No documents for device IMEI ' + deviceIMEI + ' was found in the database!');
			}else{
				var updateOperators = {
						"$set" : { "offlineFlag" : true, "lastUpdateOverspeedState" : false , "stopDetailTimerFlag" : false},
						"$push" : { "offlineAlarms" : {"offlineTime":new Date(), "offlineLocation": doc.lastUpdateLocation} },
						"$inc" : {"offlineAlarmsArraySize":1}
					};
				db.collection('devices').update(query, updateOperators, function(err, updated) {
					if(err) {
						throw err;
					}
					console.dir("Successfully updated the document of device IMEI " + deviceIMEI);
				});
			}
			
			
		});
		
	});
}

// Start a TCP Server
net.createServer(function (socket) {
	// Identify this device
	socket.name = socket.remoteAddress + ":" + socket.remotePort;
	
	var connectionInfoAdded = false;
	
	// Handle incoming messages from devices.
	socket.on('data', function (data){
		//var inMessage = data.toString('hex');// input message from device is received in Hexadecimal encoding
		var inMessage = data.toString();// input message from virtual device is received in Hexadecimal encoding
		
		var deviceIMEI = inMessage.slice(10,26);
		
		if(!connectionInfoAdded){
			var connectionInfo = new connection(socket, socket.name, socket.remoteAddress, socket.remotePort, deviceIMEI)
			// Put this new device connection in the list
			devicesConnections.push(connectionInfo);
			connectionInfoAdded = true;
		}
		
		
		var protocolNum = inMessage.slice(30,32);
		console.log("--------------------------");
		console.log(socket.name + " with IMEI " + deviceIMEI + " on protocol " + protocolNum + " > \n" + inMessage);
		
		
		switch (protocolNum.toLowerCase()) {
		case '1a': //Heartbeat packet
			socket.write('54681A0D0A', 'hex', function(){
				console.log("Heartbeat response packet: 54681A0D0A was sent to terminal");
			});
			var newUpdate = parseHeartbeatMessage(inMessage, socket);
			
			var stopDetailThreshold = 60*1000;
			handleHearbeatUpdate(newUpdate, stopDetailThreshold);
			
			break;
			
		case '10': //GPS data packet
			
			var newUpdate = parseGpsOrAddressMessage('gps',inMessage, socket);
			
			console.log("Date & time: " + newUpdate.deviceTime + "Speed: "+newUpdate.speed+" Course: "+newUpdate.direction);
			console.log("Location: " + JSON.stringify(newUpdate.location));
			
			handleGPSUpdate(newUpdate);
			
			break;
			
		case '1b': //address request packet
			var newUpdate = parseGpsOrAddressMessage('address',inMessage, socket);
			var phoneNumHex = inMessage.slice(80,122);
			newUpdate.phoneNum = miscUtils.hexString2Ascii(phoneNumHex);
			newUpdate.language = inMessage.slice(122,124);
			
			createAndSendAddressResponse(newUpdate.location,phoneNumHex,newUpdate.packetNumber,socket);
			
			handleAddressUpdate(newUpdate);
			
			break;

		default:
			console.log("No cases were hit");
			break;
		}
	});
	
	
	// Remove the device from the list when it leaves & stop any open timers or flags
	socket.on('close', function () {// 'end' event might be replaced by 'close' event [need testing]
		console.log('Closed: ' + socket.name);
		
		var socketIndex;
		var deviceIMEI;
		
		for (var i = 0; i < devicesConnections.length; i++) {
			var connection = devicesConnections[i];
			if(connection.socket == socket){
				socketIndex = i;
				deviceIMEI = connection.deviceIMEI;
				break;
			}
		}
		
		if(typeof deviceIMEI !== 'undefined'){
			devicesConnections.splice(socketIndex, 1);
			handleClosedConnection(deviceIMEI);
		}
		
	});
	
	socket.on('error', function(err){
		console.log(err);
    });
 
}).listen(portNum);
 
// Put a friendly message on the terminal of the server.
console.log("Server running at port "+portNum+"\n");



